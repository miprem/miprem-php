<?php
require_once ("./vendor/autoload.php");


class Router
{
    private string $requestPath;
    private ?array $requestParams;
    private \Miprem\Model\Poll $poll;
    private \Miprem\Model\SvgConfig $svgConfig;
    private string $tab;
    private \Twig\Environment $twig;

    const DEFAULT_TAB = 'poll';

    public function __construct() {
        $this->requestPath = trim(strtok($_SERVER['REQUEST_URI'], '?'), '/');
        parse_str($_SERVER['QUERY_STRING'] ?? '', $this->requestParams);

        $this->tab = $this->requestParams['tab'] ?? self::DEFAULT_TAB;

        $this->poll = Miprem\Model\Poll::fromQueryString($_SERVER['QUERY_STRING'] ?? '');
        if (array_key_exists('ta_poll_yaml', $this->requestParams)) {
            $ta = Miprem\Model\Poll::fromYaml($this->requestParams['ta_poll_yaml']);
            $this->poll = $this->poll->merge($ta);
        }

        $this->svgConfig = Miprem\Model\SvgConfig::fromQueryString($_SERVER['QUERY_STRING'] ?? '');
        if (array_key_exists('ta_svg_config_yaml', $this->requestParams)) {
            $ta = Miprem\Model\SvgConfig::fromYaml($this->requestParams['ta_svg_config_yaml']);
            $this->svgConfig = $this->svgConfig->merge($ta);
        }

        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/public');
        $this->twig = new \Twig\Environment($loader);
    }

    public function cleanUri()
    {
        $param_groups = [
            $this->poll->toQueryString(),
            $this->svgConfig->toQueryString(),
            $this->tab == self::DEFAULT_TAB ? '' : "tab=$this->tab"
        ];
        $str_query = join('&', array_filter($param_groups, function($s) {return $s != '';}));
        $clean_uri = '/' . $this->requestPath . (($str_query == '') ? '' : ('?' . $str_query));

        if ($_SERVER['REQUEST_URI'] != $clean_uri) {
            error_log("Redirect from:\n\t" . $_SERVER['REQUEST_URI'] . "\n\tto:\n\t" . $clean_uri);
            header("Location: $clean_uri");
            die();
        }
    }

    public function route() : string
    {
        switch ($this->requestPath) {
            case '':
            case 'demo.php':
                $this->cleanUri();
                header("Content-Type: text/html");
                return $this->index();
                break;
            case 'svg':
                $this->cleanUri();
                header("Content-Type: image/svg+xml");
                return $this->svg();
                break;
            case 'png':
                $this->cleanUri();
                header("Content-Type: image/png");
                return $this->png();
                break;
            case 'html':
                $this->cleanUri();
                header("Content-Type: text/html");
                return $this->html();
                break;
            default:
                $file_path = get_static_path($this->requestPath);
                $mime = get_mime($file_path);
                if ($file_path && $mime) {
                    header("Content-Type: $mime");
                    return file_get_contents($file_path);
                } else {
                    header("Content-Type: text/html");
                    return $this->notFound();
                }
                break;
        }
    }

    public function index() : string
    {
        $config = $this->svgConfig;
        return $this->twig->load('index.html')->render([
            'server_name' => 'Miprem PHP',
            'tab' => $this->requestParams['tab'] ?? 'poll',
            'title' => $this->poll->getSubject()['label'],
            'poll_yaml' => $this->poll->toYaml(),
            'svg_config_yaml' => $config->setCustomCss('')->toYaml(),
            'custom_svg_css' => Miprem\Model\SvgConfig::prettifyCss($this->svgConfig->getCustomCss()),
            'mp_svg' => $this->svg(),
            'og' => $this->getOpenGraph()
        ]);
    }

    public function svg() : string
    {
        $renderer = new Miprem\Renderer\SvgRenderer($this->svgConfig);
        return $this->poll->render($renderer);
    }

    public function png()
    {
        $renderer = new Miprem\Renderer\PngGDRenderer($this->svgConfig);
        return $this->poll->render($renderer);
    }

    public function html() : string
    {
        return $this->twig->load('merit_profile.html')->render([
            'title' => $this->poll->getSubject()['label'],
            'mp_svg' => $this->svg(),
            'og' => $this->getOpenGraph()
        ]);
    }

    public function notFound() : string
    {
        $this->tplData['error'] = 'Page not found.';
        http_response_code(404);
        return $this->index();
    }

    private function getOpenGraph() : string
    {
        $renderer = new Miprem\Renderer\OpenGraphRenderer();

        $png_url = "http://{$_SERVER['SERVER_NAME']}/png?" . http_build_query([
            'poll' => $this->poll->toQueryString(),
            'svg_config' => $this->svgConfig->toQueryString()
        ]);
        $opt = ['png_url' => $png_url];
        return $this->poll->render($renderer, $opt);
    }

}

function get_static_path(string $request_path) : string
{
    $file_path = realpath(__DIR__ . '/public/' . $request_path);
    $pub_dir = realpath(__DIR__ . '/public/');
    $is_prefix_safe = substr_compare($file_path, $pub_dir, 0, strlen($pub_dir)) === 0;
    return ($is_prefix_safe && file_exists($file_path)) ? $file_path : '';
}

function get_mime(string $file_path)
{
    $ext = pathinfo($file_path, PATHINFO_EXTENSION);
    $supported_mimes = [
        'html' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/x-javascript'
    ];
    return $supported_mimes[$ext] ?? '';
}

function dump($object, string $name = 'obj')
{
    highlight_string("<?php\n\$$name = " . var_export($object, true) . ";\n");
}

$router = new Router();
echo $router->route();
