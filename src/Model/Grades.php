<?php

namespace Miprem\Model;

abstract class Grades
{
    const DEFAULT_GRADES = [
        [
            ['label' => 'Poor'],
            ['label' => 'Good']
        ],
        [
            ['label' => 'Poor'],
            ['label' => 'Fair'],
            ['label' => 'Good']
        ],
        [
            ['label' => 'Reject'],
            ['label' => 'Poor'],
            ['label' => 'Good'],
            ['label' => 'Excellent']
        ],
        [
            ['label' => 'Reject'],
            ['label' => 'Poor'],
            ['label' => 'Fair'],
            ['label' => 'Good'],
            ['label' => 'Excellent']
        ],
        [
            ['label' => 'Reject'],
            ['label' => 'Insufficient'],
            ['label' => 'Poor'],
            ['label' => 'Good'],
            ['label' => 'Very good'],
            ['label' => 'Excellent']
        ],
        [
            ['label' => 'Reject'],
            ['label' => 'Insufficient'],
            ['label' => 'Poor'],
            ['label' => 'Fair'],
            ['label' => 'Good'],
            ['label' => 'Very good'],
            ['label' => 'Excellent']
        ],
    ];

    public static function fromAmountOfGrades(int $amountOfGrades) : array
    {
        return self::DEFAULT_GRADES[$amountOfGrades - 2];
    }

    public static function fromTally(array $tally) : array
    {
        return self::fromAmountOfGrades(self::getAmountOfGrades($tally));
    }

    public static function fromBroadGrades($grades, array $tally) : array
    {
        if ($grades == null) {
            $grades = self::fromTally($tally);
        } else if (gettype($grades) == 'integer') {
            $grades = self::fromAmountOfGrades($grades);
        }
        $calculatedAmountOfGrades = self::getAmountOfGrades($tally);
        if (count($grades) < $calculatedAmountOfGrades) {
            for ($i=count($grades); $i < $calculatedAmountOfGrades; $i++) {
                $grades[$i] = '???';
            }
        }
        return $grades;
    }

    public static function getAmountOfGrades(array $tally) : int
    {
        return max(array_map(function($pt) { return count($pt); }, $tally));
    }

}
