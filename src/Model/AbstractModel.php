<?php

namespace Miprem\Model;


abstract class AbstractModel
{

    public abstract static function sample() : self;
    public abstract static function fromArray(array $pollArray) : self;
    public abstract static function fromQueryString(string $pollQueryString) : self;
    public abstract function toArray() : array;
    public abstract function toQueryString() : string;
    public abstract function merge(self $that) : self;

}
