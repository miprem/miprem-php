<?php

namespace Miprem\Renderer;

use Twig\Twig;

class OpenGraphRenderer extends AbstractRenderer
{
    const DEFAULT_IMAGE_WIDTH = 1200;
    const DEFAULT_IMAGE_HEIGHT = 630;

    private int $width;
    private int $height;
    private \Twig\TemplateWrapper $template;

    public function __construct(
        int $width = self::DEFAULT_IMAGE_WIDTH,
        int $height = self::DEFAULT_IMAGE_HEIGHT)
    {
        $this->width = $width;
        $this->height = $height;

        $this->initTwig();
        $this->template = $this->twig->load('merit_profile_og.xml');
    }

    public function render(\Miprem\Model\Poll $poll, array $opt = []) : string
    {
        $poll_label = $poll->getSubject()['label'];
        $image = [
            'url' => $opt['png_url'] ?? '',
            'type' => 'image/png',
            'width' => $this->width,
            'height' => $this->height,
            'alt' => "The merit profile of the poll \"$poll_label\"."
        ];

        return $this->template->render([
            'title' => "$poll_label | merit profile",
            'description' => "The merit profile of the poll \"$poll_label\".",
            'site_name' => 'Miprem',
            'locale' => 'en',
            'image' => $image
        ]);
    }

    public function getIdentifier(\Miprem\Model\Poll $poll, array $opt = []) : string
    {
        return md5(json_encode([
            'width' => $this->width,
            'height' => $this->height,
            'poll' => $poll->toArray(),
            'opt' => $opt
        ]));
    }

    public function getFileExtension() : string
    {
        return '.og.xml';
    }

}
