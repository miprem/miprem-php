<?php

namespace Miprem\Renderer;

use Twig\Twig;

abstract class AbstractRenderer
{

    const TEMPLATES_DIR = __DIR__ . '/../../templates';

    protected \Twig\Environment $twig;

    public abstract function render(\Miprem\Model\Poll $poll, array $opt = []) : string;
    public abstract function getIdentifier(\Miprem\Model\Poll $poll, array $opt = []) : string;
    public abstract function getFileExtension() : string;

    protected function initTwig()
    {
        $loader = new \Twig\Loader\FilesystemLoader(self::TEMPLATES_DIR);
        $this->twig = new \Twig\Environment($loader, ['strict_variables' => True]);
        $this->twig->addFunction(new \Twig\TwigFunction('concat', [$this, 'concat']), ['is_variadic' => true]);
    }

    public function save(\Miprem\Model\Poll $poll, string $dirPath, array $opt = []) : string
    {
        if( ! is_dir($dirPath)) {
            mkdir($dirPath, 0777, true);
            error_log('Created directory at ' . realpath($dirPath));
        }

        $file_name = $this->getIdentifier($poll, $opt) . $this->getFileExtension();
        $file_path = realpath($dirPath) . '/' . $file_name;

        if(is_file($file_path)) {
            error_log('File already exists at ' . $file_path . ', skipping.');
        } else {
            error_log('Saved ressource at ' . $file_path);
            file_put_contents($file_path, $this->render($poll, $opt));
        }

        return $file_path;
    }

    public function concat(...$words) : string
    {
        return join("", $words);
    }

    public function getTemplatesDir() : string
    {
        return self::TEMPLATES_DIR;
    }

}
