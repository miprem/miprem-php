# Miprem - PHP implementation

The PHP implementation of [Miprem](https://roipoussiere.frama.io/miprem/).

## Installation

### Prerequisites

You must install PHP and Composer (ie. `sudo apt install php composer`).

### Clone repository

    git clone --recurse-submodules https://framagit.org/roipoussiere/miprem-php.git
    cd miprem-php

Notice the `--recurse-submodules` that clones the submodules (miprem and miprem-client-demo).

At any moment you can update the submodules with:

    git submodule update --init --recursive

Or:

    cd miprem && git pull && cd ..

### Bind templates folder

To update templates folder, mount it from the miprem submodule:

    ./bin/mount-templates.sh

Templates are added to git because Composer can not clone a git repo with its submodules.

### Install dependencies

    composer install

Then you may want to install optional PHP extensions:

- **ext-yaml**: Needed by Poll and svgConfig to instantiate classes from Yaml;
- **ext-simplexml**(__*__): Needed by PngGDRenderer to read xml files;
- **ext-gd**(__*__): Needed by PngGDRenderer to rasterize svg images;
- **ext-imagick**(__*__): Needed by PngIMRenderer to rasterize svg images.

On Debian-based systems, they can be installed via packages `php-yaml`, `php-xml`, `php-gd`, `php-imagick`, respectively.

(__*__) To generate png, you have the choice between two renderers:

- `PngIMRenderer` (recommended), that use the extension *ext-imagick*;
- `PngGDRenderer`, that use the package *php-svg*, which require *ext-simplexml* and *ext-gd* extensions.

The first one is recommended as it produce better results, the second one is lighter and faster but generated images can be glitched.

### Test with demo client

Now Miprem PHP is installed, you can test it by running the demo web client:

    ./bin/serve.sh

Then open http://127.0.0.1:8000/demo.php.

### Building the library

If you work on miprem.php, you must run `build.sh` from the miprem-js root folder after your edit.

```bash
./build.sh
```

It generates php files based on a template and values:

- src/Template.php: based on TemplateEmpty.php, with the {{ TEMPLATE_NAME }} occurences replaced by their corresponding file content.
- src/DefaultData.php: based on DefaultDataEmpty.php, with the {{ DEFAULT_DATA }} occurences replaced by their corresponding file content.

## Licence & authorship

This project is developed by the [MieuxVoter non-profit organization](http://mieuxvoter.fr/) and published under [MIT licence](./LICENCE).
